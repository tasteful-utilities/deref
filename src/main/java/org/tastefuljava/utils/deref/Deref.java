package org.tastefuljava.utils.deref;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Deref {
    private static final Logger LOG = Logger.getLogger(Deref.class.getName());

    public static void main(String[] args) {
        for (String uri: args) {
            try {
                URL url = new URL(uri);
                do {
                    System.out.println(url);
                    url = deref(url);
                } while (url != null);
            } catch (MalformedURLException ex) {
                LOG.log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                LOG.log(Level.SEVERE, null, ex);
            }
        }
    }

    private static URL deref(URL url) throws IOException {
        HttpURLConnection cnt = (HttpURLConnection) url.openConnection();
        cnt.setInstanceFollowRedirects(false);
        cnt.setRequestMethod("HEAD");
        int status = cnt.getResponseCode();
        if (status >= 301 && status <= 308) {
            return new URL(url, cnt.getHeaderField("Location"));
        }
        return null;
    }
}
